def createAccount(number,ownerName,balance,limit):
    account = {
        "number": number,
        "ownerName": ownerName,
        "balance": balance,
        "limit": limit
    }
    return account

def deposit(account,value):
    account["balance"] += value

def withdraw(account, value):
    account["balance"] -= value

def balance(account):
    print("Your money balance is ${}".format(account["balance"]))
