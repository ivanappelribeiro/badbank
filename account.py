class Account:
    def __init__(self,number,ownerName,balance,limit):
        print("Building object... {}".format(self))
        self.__number = number
        self.__ownerName = ownerName
        self.__balance = balance
        self.__limit = limit

    def balance(self):
        print(f'{self.__ownerName}, your money balance is ${self.__balance}')

    def deposit(self,value):
        self.__balance += value

    def withdraw(self, value):
        self.__balance -= value

    def transfer(self,value,targetAccount):
        self.withdraw(value)
        targetAccount.deposit(value)
